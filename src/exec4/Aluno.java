package exec4;

public class Aluno implements Comparable<Aluno>{

	private String nome;
	private Integer idade;

	public Aluno(String nome, int idade) {
		super();
		this.nome = nome;
		this.idade = idade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	@Override
	public String toString() {
		return "Aluno [nome=" + nome + ", idade=" + idade + "]";
	}

	@Override
	public int compareTo(Aluno o) {
		if (this.getIdade()<o.getIdade()) {
			return -1;
		}else if (this.getIdade()>o.getIdade()) {
			return 1;
		}
		return 0;
	}
	
	

}
