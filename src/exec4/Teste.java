package exec4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;

public class Teste {

	private static Scanner sc = new Scanner(System.in);
	private static Queue<Aluno> alunos = new LinkedList<Aluno>();
	public static void main(String[] args) {
		do {

			System.out.print("\n1)Inserir aluno \n2)Consultar alunos \n3)Sair \nInforme o que deseja fazer: ");
			int op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1: {
				adicionarAluno();
				break;
			}
			case 2: {
				consultarAluno();
				break;
			}
			case 3: {
				System.out.print("At� a pr�xima! :P: ");
				System.exit(0);
				break;
			}
			default:
				System.out.println("Op��o n�o registrada!");
			}

		} while (true);

	}

	public static void adicionarAluno() {
		System.out.println("Informe o nome do aluno");
		String nome = sc.nextLine();
		System.out.println("Informe a idade do aluno");
		int idade = sc.nextInt();
		sc.nextLine();

		Aluno a = new Aluno(nome, idade);
		alunos.add(a);
		sortedAlunos();
	}
	
	private static void sortedAlunos() {
		List<Aluno> list = new ArrayList<Aluno>();
		for (Aluno aluno : alunos) {
			list.add(aluno);
		}
		list.sort((a,b) -> Integer.compare(a.getIdade(), b.getIdade()));
		alunos.clear();
		for (Aluno aluno : list) {
			alunos.add(aluno);
		}
	}

	public static void consultarAluno() {
		Iterator<Aluno> alunosIte = alunos.iterator();
		while (alunosIte.hasNext()) {
			System.out.println(alunosIte.next());
		}
	}
}
