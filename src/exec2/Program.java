package exec2;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Program {

	private static Scanner sc = new Scanner(System.in);
	private static Queue<Contato> contatos = new LinkedList<Contato>();
	
	public static void main(String[] args) {
		
		do {
			
			System.out.print("\n1)Inserir Contato \n2)Remover Contato \n3)Sair \nInforme o que deseja fazer: ");
			int op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1: {
				adicionarContato();
				break;
			}
			case 2: {
				proximoContato();
				break;
			}
			case 3: {
				System.out.print("At� a pr�xima! :P: ");
				System.exit(0);
				break;
			}
			default:
				System.out.println("Op��o n�o registrada!");
			}
			
		} while (true);
		

	}
	
	public static void adicionarContato() {
		System.out.println("Informe o nome do contato");
		String nome = sc.nextLine();
		System.out.println("Informe o n�mero do contato");
		String numero = sc.nextLine();
		
		Contato c = new Contato(nome, numero);
		contatos.add(c);
	}
	
	public static void proximoContato() {
		System.out.println(contatos.poll());
	}

}
