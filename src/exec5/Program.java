package exec5;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Program {

	private static Scanner sc = new Scanner(System.in);
	private static Queue<String> pacientes = new LinkedList<String>();

	public static void main(String[] args) {
		do {

			System.out.print(
					"\n1)Inserir paciente \n2)Atender paciente \n3)Verificar se h� pacientes \n4)Indicar o pr�ximo paciente \n5)Quantidades de pacientes \n6)Sair \nInforme o que deseja fazer: ");
			int op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1: {
				adicionarPaciente();
				break;
			}
			case 2: {
				atenderPaciente();
				break;
			}
			case 3: {
				verificarPacientes();
				break;
			}
			case 4: {
				indicaProximo();
				break;
			}
			case 5: {
				System.out.println("Existem: "+pacientes.size()+" pacientes na fila");
				break;
			}
			case 6: {
				System.out.print("At� a pr�xima! :P: ");
				System.exit(0);
				break;
			}
			default:
				System.out.println("Op��o n�o registrada!");
			}

		} while (true);

	}

	private static void indicaProximo() {
		System.out.println("Paciente "+pacientes.peek()+" para atendimento");
	}

	private static void verificarPacientes() {
		System.out.println((pacientes.isEmpty()) ? "N�o h� mais paci�ntes" : "Existe pacientes na fila");
		
	}

	public static void adicionarPaciente() {
		System.out.println("Informe o nome do paciente");
		String nome = sc.nextLine();
		pacientes.add(nome);
	}

	public static void atenderPaciente() {
		System.out.println("Paciente "+pacientes.poll()+" foi atendido");
	}

}
