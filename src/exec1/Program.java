package exec1;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Program {

	private static Queue<String> filaMusicas = new LinkedList<>();
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		do {
			System.out.print("\n1)Inserir m�sica \n2)Consultar m�sica \n3)Remover m�sica \n4)Sair \nInforme o que deseja fazer: ");
			int op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1: {
				System.out.print("Informe o nome da m�sica: ");
				String musica = sc.nextLine();
				filaMusicas.add(musica);
				break;
			}
			case 2: {
				System.out.print("Informe o nome da m�sica para consulta: ");
				String musica = sc.nextLine();
				String resp = (filaMusicas.contains(musica))? "Existe essa m�sica na fila":"Essa m�sica n�o est� na fila";
				System.out.println(resp);
				break;
			}
			case 3: {
				System.out.print("Informe o nome da m�sica a ser removida: ");
				String musica = sc.nextLine();
				String resp = (filaMusicas.remove(musica))? "M�sica removida da fila":"Essa m�sica n�o est� na fila";
				System.out.println(resp);
				break;
			}
			case 4: {
				System.out.print("At� a pr�xima! :P: ");
				System.exit(0);
				break;
			}
			default:
				System.out.println("Op��o n�o registrada!");
			}
			
			
		} while (true);

	}

}
